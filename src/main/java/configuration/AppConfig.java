package configuration;

import calculator.Calculator;
import interfaces.Command;
import operations.Addition;
import operations.Division;
import operations.Multiplication;
import operations.Subtraction;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class AppConfig {
    @Bean
    public List<Command> commands() {
        return Arrays.asList(new Addition(), new Subtraction(), new Multiplication(), new Division());
    }

    @Bean
    public Calculator calculator(List<Command> commands) {
        return new Calculator(commands);
    }

    @Bean
    public ApplicationRunner applicationRunner(Calculator calculator) {
        return args -> {
            calculator.calculate(10, 20, '+');
            calculator.calculate(100, 20, '/');
            calculator.calculate(10, 3, '*');
            calculator.calculate(10, 3, '-');
            calculator.calculate(10.5, 3, '/');
        };
    }
}