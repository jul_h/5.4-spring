package calculator;

import interfaces.Command;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Calculator {
    private Map<Character, Command> commands;

    public Calculator(List<Command> commandList) {
        commands = new HashMap<>();
        for (Command command : commandList) {
            commands.put(command.getOperator(), command);
        }
    }

    public void calculate(double a, double b, char operator) {
        Command command = commands.get(operator);
        if (command != null) {
            double result = command.execute(a, b);
            System.out.printf("%.2f %c %.2f = %.2f\n", a, operator, b, result);
        } else {
            System.err.println("Invalid operation: " + a + " " + operator + " " + b);
        }
    }
}
