package interfaces;

public interface Command {
    char getOperator();
    double execute(double a, double b);
}
