package main;

import calculator.Calculator;
import configuration.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        Calculator calculator = context.getBean(Calculator.class);
        calculator.calculate(10, 20, '+');
        calculator.calculate(100, 20, '/');
        calculator.calculate(10, 3, '*');
        calculator.calculate(10, 3, '-');
        calculator.calculate(10.5, 3, '/');
        calculator.calculate(10.5, 3, '%');
    }
}
